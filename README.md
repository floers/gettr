# gettr

gettr is a i18n framework for rust.

It's initial objective is to support i18n for yew applications.

## Getting Started

Add gettr as dependency and as build-dependency.

```Cargo.toml
[dependencies]
gettr = "0.0.1"
[build-dependencies]
gettr = "0.0.1"
```

add build step to `build.rs`:

```rs
use std::path::PathBuf;
pub fn main() -> std::io::Result<()> {
    gettr::update_translations(
        // list all languages you want to support here
        // each language will create a file in the destination directory
        vec!["de"],

        // The directory to search for message to translate
        PathBuf::from("src"),

        // destination directory
        PathBuf::from("src/i18n")
    )?;
    Ok(())
}
```

_build.rs_

use gettr

```rs
pub fn main() {
    // init gettr once at start
    gettr::init("en", vec![
        ("de", include_str!("i18n/de"))
    ]);

    // print a message based on the language
    // gettr! can handle {} placeholders ATM
    println!("{}", gettr::gettr!("Hello World! {}", 18));
}
```

_src/main.rs_

gettr will search all files in `src` and find calls of the `gettr!` macro. The default language is used as translation key and fallback. For each call a translation file is generated in `src/i18n`. In this case the following file is generated:

```js
// This file is generated and automatically updated by gettr.
// Values set to keys will be kept on updates.

// src/main.rs#8
"Hello World! {}" = ""
```

_src/i18n/de_

You can maintain the translation for `Hello World!` in this file. It will be automatically updated on every build. Existing keys will be kept. If a maintained key can no longer be found in the code it is removed.

## Integration with yew

Using the `yew` feature you can use the provided `GettrContext` and `use_gettr` to integrate gettr in your yew client.

```rs
use routing::Routing;
use yew::{function_component, html, Html};

use gettr::GettrProvider;

fn main() {
    gettr::init("en", vec![
        ("de", include_str!("i18n/de"))
    ]);
    yew::Renderer::<App>::new().render();
}

#[function_component(App)]
pub fn app() -> Html {
    html! {
        <GettrProvider>
            <Home></Home>
        </GettrProvider>
    }
}

use gettr::{use_gettr, gettr};

#[function_component(Home)]
pub fn home() -> Html {
    use_gettr() // call the hook to ensure your component updates when the language is changed
    html! {
        <div>
            {gettr!("Hello World!")}
        </div>
    }
}
```

## Limitations

### formatting

gettr uses a regular expression to find `regex!` calls in the code. This means it won't work well with formatting these calls in more than one line.

Good: 
```rs
gettr!("This is an example {}", 124);
```
Bad:
```rs
gettr!(
    "This is an example {}",
    124
);
```

### placeholders

Rusts fmt macros allow named arguments. gettr can not handle them ATM.

### plurals

gettr has no support for plural forms other than specifing them manually.