use yew::{
    function_component, hook, html, use_context, use_effect_with, use_force_update, use_state,
    Children, ContextProvider, Html, Properties, UseStateHandle,
};

#[derive(Debug, Clone, PartialEq)]
pub struct GettrContext {
    inner: UseStateHandle<String>,
}

impl GettrContext {
    pub fn new(inner: UseStateHandle<String>) -> Self {
        Self { inner }
    }

    pub fn set(&self, language: String) {
        self.inner.set(language);
    }

    pub fn get(&self) -> String {
        (*self.inner).clone()
    }
}

#[derive(Debug, PartialEq, Properties)]
pub struct GettrLanguageProvider {
    pub children: Children,
}

#[function_component(GettrProvider)]
pub fn gettr_provider(props: &GettrLanguageProvider) -> Html {
    let language = use_state(|| String::default());
    let ctx = GettrContext::new(language.clone());
    use_effect_with(ctx.get(), move |l| {
        crate::update(l);
    });
    html! {
        <ContextProvider<GettrContext> context={ctx}>
            {props.children.clone()}
        </ContextProvider<GettrContext>>
    }
}

#[hook]
pub fn use_gettr() -> GettrContext {
    let ctx = use_context::<GettrContext>().unwrap();
    let trigger = use_force_update();
    use_effect_with(ctx.get(), move |_| {
        trigger.force_update();
    });
    ctx
}
